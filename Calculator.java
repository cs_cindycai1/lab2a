import java.util.Random;

public class Calculator {
	public static int add(int num1, int num2) {
		return num1 + num2;
	}
	
	public static double sqrt(int num) {
		return Math.sqrt(num); //Math
	}
	
	public static int randomNum() {
		Random rand = new Random();
		return rand.nextInt(20); //Random
	}
	
	public static int divide(int num1, int num2) {
		if (num2 == 0) {
			System.out.print("Undefined number  "); //System??
			return -10000000;
		}
		return num1 / num2;
	}
}