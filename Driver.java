import java.util.Scanner;

public class Driver {
	public static void main(String[] args) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		System.out.println("Hello"); //System
		System.out.println("Enter 2 numbers you wish to add"); //System
		int num1 = reader.nextInt(); //Scanner
		int num2 = reader.nextInt(); //Scanner
		System.out.println(Calculator.add(num1, num2)); // Calculator & System
		System.out.println("Input a number"); //System
		num1 = reader.nextInt(); //Scanner
		System.out.println(Calculator.sqrt(num1)); // Calculator & System
		System.out.println("Random number: " + Calculator.randomNum()); // Calculator & System
	}
}